package com.iteaj.iot;

/**
 * 端口类型
 */
public enum PortType {
    Tcp, Udp
}
