package com.iteaj.iot.udp;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.message.DefaultMessageHead;

public class UdpMessageHead extends DefaultMessageHead {

    public UdpMessageHead(byte[] message) {
        super(message);
    }

    public UdpMessageHead(String equipCode, byte[] message) {
        this(equipCode, null, message);
    }

    public UdpMessageHead(String equipCode, ProtocolType type) {
        super(equipCode, type);
    }

    public UdpMessageHead(String equipCode, ProtocolType type, byte[] message) {
        super(equipCode, type, message);
    }

    public UdpMessageHead(String equipCode, String messageId, ProtocolType type) {
        super(equipCode, messageId, type);
    }

    public UdpMessageHead(String equipCode, String messageId, ProtocolType type, byte[] message) {
        super(equipCode, messageId, type, message);
    }
}
