package com.iteaj.iot.server.dtu.message;

import com.iteaj.iot.Message;

public class DtuPassMessage extends DtuServerMessageAbstract{

    protected static DtuPassMessage passMessage = new DtuPassMessage();

    public static DtuPassMessage getInstance() {
        return passMessage;
    }

    protected DtuPassMessage() {
        super(Message.EMPTY);
    }

    @Override
    protected MessageHead doBuild(byte[] message, String equipCode) {
        return null;
    }
}
