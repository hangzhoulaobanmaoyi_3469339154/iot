package com.iteaj.iot.test.client.fixed;

import com.iteaj.iot.CoreConst;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.codec.FixedLengthFrameClient;
import com.iteaj.iot.client.component.FixedLengthFrameClientComponent;
import com.iteaj.iot.client.component.TcpClientComponent;
import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.test.ClientSnGen;
import com.iteaj.iot.test.IotTestProperties;
import com.iteaj.iot.test.TestMultiClientManager;
import com.iteaj.iot.test.TestProtocolType;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

public class FixedLengthClientComponent extends FixedLengthFrameClientComponent<FixedLengthClientMessage> {

    public FixedLengthClientComponent(ClientConnectProperties config) {
        super(config, 28);
    }

    @Override
    public String getName() {
        return "固定长度字段解码";
    }

    @Override
    public String getDesc() {
        return "用于测试客户端固定长度解码器[FixedLengthFrameDecoder]";
    }

    @Override
    public TcpSocketClient createNewClient(ClientConnectProperties config) {
        return new FixedLengthFrameClient(this, config, this.getFrameLength()) {
            @Override
            protected void doInitChannel(Channel channel) {
                super.doInitChannel(channel);
                IotTestProperties.TestMultiConnectConfig connectConfig = (IotTestProperties.TestMultiConnectConfig) config;
                channel.attr(CoreConst.EQUIP_CODE).set(connectConfig.getDeviceSn());
            }
        };
    }

    @Override
    public AbstractProtocol getProtocol(FixedLengthClientMessage message) {
        TestProtocolType type = message.getHead().getType();
        if(type == TestProtocolType.CIReq) {
            return remove(message.getHead().getMessageId());
        } else if(type == TestProtocolType.PIReq) {
            return new FixedLengthServerRequestProtocol(message);
        }

        return null;
    }

    @Override
    protected ServerInitiativeProtocol doGetProtocol(FixedLengthClientMessage message, ProtocolType type) {
        return null;
    }
}
