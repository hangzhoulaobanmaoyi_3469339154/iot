package com.iteaj.iot.test.client.udp;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.test.TestProtocolType;

public class UdpClientTestInitProtocol extends ClientInitiativeProtocol<UdpClientTestMessage> {

    private byte[] message;

    public UdpClientTestInitProtocol(byte[] message) {
        this.message = message;
    }

    @Override
    protected UdpClientTestMessage doBuildRequestMessage() {
        return new UdpClientTestMessage(new DefaultMessageHead("123456", protocolType(), this.message));
    }

    @Override
    public void doBuildResponseMessage(UdpClientTestMessage responseMessage) {

    }

    @Override
    public ProtocolType protocolType() {
        return TestProtocolType.CIReq;
    }
}
