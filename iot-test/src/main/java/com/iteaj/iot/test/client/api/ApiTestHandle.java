package com.iteaj.iot.test.client.api;

import com.iteaj.iot.FrameworkManager;
import com.iteaj.iot.IotThreadManager;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.consts.ExecStatus;
import com.iteaj.iot.server.ServerComponent;
import com.iteaj.iot.test.IotTestHandle;
import com.iteaj.iot.test.IotTestProperties;
import com.iteaj.iot.test.server.api.ApiTestServerMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

import static com.iteaj.iot.test.TestConst.LOGGER_API_DESC;

@Component
@ConditionalOnExpression("${iot.test.client:false}")
public class ApiTestHandle implements IotTestHandle {

    @Autowired
    private IotTestProperties properties;
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void start() throws Exception {
        System.out.println("-------------------------------------------- FrameworkManager Api测试 ---------------------------------------------");
        ClientConnectProperties connectProperties = new ClientConnectProperties(properties.getHost(), properties.getApiPort());
        ServerComponent serverComponent = FrameworkManager.getServerComponent(ApiTestServerMessage.class);
        if(serverComponent != null) {
            // 组件注册测试
            FrameworkManager.getInstance()
                    .register(new ApiTestClientComponent(connectProperties))
                    .start();

            try {
                new ApiStopTestProtocol().timeout(8000).request(protocol -> {
                    if(protocol.getExecStatus() == ExecStatus.success) {
                        logger.info(LOGGER_API_DESC, "FrameworkManager", "register(FrameworkComponent) + start", "通过");
                        boolean close = FrameworkManager.getInstance().stop(ApiTestClientMessage.class);
                        if(close) {
                            IotThreadManager.instance().getExecutorService().schedule(() -> {
                                FrameworkManager.getInstance().start(ApiTestClientMessage.class);

                                // 组件关闭测试
                                new ApiCloseTestProtocol().request(protocol1 -> {
                                    if(protocol1.getExecStatus() == ExecStatus.success) {
                                        FrameworkManager.getInstance().close(ApiTestClientMessage.class);
                                    }
                                });
                            }, 3, TimeUnit.SECONDS);
                        }
                    } else {
                        logger.error(LOGGER_API_DESC, "FrameworkManager"
                                , "register(FrameworkComponent) + start", "失败("+protocol.getExecStatus().desc+")");
                    }
                });

            } catch (Exception e) {
                logger.error(LOGGER_API_DESC, "FrameworkManager", "register(FrameworkComponent) + start", "失败", e);
            }
        }

        TimeUnit.SECONDS.sleep(8);
    }

    @Override
    public int getOrder() {
        return 100;
    }
}
