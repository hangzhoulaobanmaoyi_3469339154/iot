package com.iteaj.iot.taos;

import com.iteaj.iot.tools.db.ParamValue;

public class SqlExecContext {

    private StringBuilder sql;

    private ParamValue[] values;


    public SqlExecContext(StringBuilder sql, ParamValue[] values) {
        this.sql = sql;
        this.values = values;
    }

    public StringBuilder getSql() {
        return sql;
    }

    public void setSql(StringBuilder sql) {
        this.sql = sql;
    }

    public ParamValue[] getValues() {
        return values;
    }

    public void setValues(ParamValue[] values) {
        this.values = values;
    }
}
